import localforage from 'localforage';

let totalCardsData = email => {
	return localforage.createInstance({
		name: email,
		storeName: 'cards',
	});
};

export default totalCardsData;

// let totalCardsData = localforage.createInstance({
// 	name: 'totalCardsData',
// 	storeName: 'cards',
// });

// export { totalCardsData };
