import firebase from 'firebase/app';
import 'firebase/database';
import 'firebase/auth';
import 'firebase/firestore';
import initFirebase from '../utils/auth/initFirebase';
import 'firebase/functions';
// import { totalCardsData } from '../Stores/IndexdDBFunctions';
import something from '../Stores/IndexdDBFunctions';
import { parseCookies } from 'nookies';
import Data from '../components/Data';
import totalCardsData from '../Stores/IndexdDBFunctions';

initFirebase();

let McqStore = [],
	infoAboutTheTest = [],
	answers = [];

let defaultDatabase = firebase.database();
let testResultStore = firebase.firestore();

function clearStorage() {
	McqStore = [];
	infoAboutTheTest = [];
}

export async function sendDataToFirebase(changePage) {
	let filteredData = [];
	let id, email;
	const cookies = parseCookies();
	const userDataFromCookie = cookies.user;
	if (userDataFromCookie) {
		const userData = JSON.parse(userDataFromCookie);
		id = userData.uid;
		email = userData.email;
	}

	McqStore = McqStore.filter(item => item.question !== '');
	let modifiedEmail = email.replace(/\./g, ',');
	for (const [key, value] of Object.entries(McqStore)) {
		if (Object.entries(value).length !== 0) {
			let data = value.options.filter(item => item.option !== '');
			if (data.length !== 0) {
				filteredData.push(data);
			}
		}
	}

	McqStore.shift();
	console.log('filtered data ==== >', filteredData);
	filteredData.forEach((item, index) => {
		McqStore[index].options = filteredData[index];
	});

	let data = {
		[infoAboutTheTest.title]: {
			infoAboutTheTest,
			McqStore,
		},
	};

	const newRef = defaultDatabase.ref(modifiedEmail).push(data);
	let newID = newRef.key;
	localStorage.setItem('testKey', newID);

	totalCardsData(email).setItem(newID, data);

	const testData = McqStore.map(item => {
		const question = item.question;
		let options;
		if (item.options) {
			options = item.options.map(item => [item.option, item.description]);
		}

		return [question, options];
	});

	const object = Object.assign({}, [testData, id]);

	if (newID) {
		defaultDatabase.ref('/test').child(newID).set(object);
	}

	const testAnswer = McqStore.map(item => {
		const question = item.question;
		let options;
		if (item.options) {
			options = item.options.filter(item => item.answer === 'correct');
		}

		return [question, options];
	});

	const testAnswerObject = Object.assign({}, testAnswer);

	testAnswerObject.passingPercentage = infoAboutTheTest.score;
	testAnswerObject.title = infoAboutTheTest.title;

	if (newID) {
		defaultDatabase.ref('/answers').child(newID).set(testAnswerObject);
	}

	if (newID) {
		let downloadCount = defaultDatabase.ref(`${modifiedEmail}/count`);
		let snapshotDownloaded = await downloadCount.once('value');
		let count = snapshotDownloaded.val();
		count += 1;
		let uploadCount = defaultDatabase.ref(`${modifiedEmail}/count`);
		let snapshotUploaded = await uploadCount.set(count);
	}

	infoAboutTheTest = [];
	McqStore = [];
	changePage(1);
}

export async function updateDataOnDatabase(key, data) {
	let id, email;
	const cookies = parseCookies();
	const userDataFromCookie = cookies.user;
	if (userDataFromCookie) {
		const userData = JSON.parse(userDataFromCookie);
		id = userData.uid;
		email = userData.email;
	}

	let modifiedEmail = email.replace(/\./g, ',');

	localStorage.setItem('testKey', key);

	const name = Object.keys(data)[0];

	const testData = data[name].McqStore;

	const testPages = [];

	testData.forEach(item => {
		if (Object.entries(item).length !== 0) {
			const question = item.question;
			let options;
			if (item.options) {
				options = item.options.map(item => [item.option, item.description]);
			}

			testPages.push([question, options]);
		}
	});

	const object = Object.assign({}, [testPages, id]);

	totalCardsData(email).setItem(key, data);
	if (id) {
		defaultDatabase.ref('/' + modifiedEmail + '/' + key).set(data);
		defaultDatabase.ref('/test').child(key).set(object);
	}

	const testAnswer = data[name].McqStore.map(item => {
		const question = item.question;
		let options;
		if (item.options) {
			options = item.options.filter(item => item.answer === 'correct');
		}

		return [question, options];
	});

	testAnswer.shift();

	const testAnswerObject = Object.assign({}, testAnswer);

	testAnswerObject.passingPercentage = data[name].infoAboutTheTest.score;
	testAnswerObject.title = data[name].infoAboutTheTest.title;

	if (key) {
		defaultDatabase.ref('/answers').child(key).set(testAnswerObject);
	}
}

export async function storeCandidateTestAnswers(id, answers, userName, email) {
	let error = false;
	try {
		const result = testResultStore
			.collection(email)
			.doc(id)
			.set({ ...answers });
		let errorObjectIfErrorIsReturned = await result;
	} catch (error) {
		let firebasePermissionError = JSON.parse(JSON.stringify(error));
		if (firebasePermissionError.code === 'permission-denied') {
			window.location = window.location.origin + '/you_cant_do_that';
		}
		error = true;
	}

	if (!error) {
		const calc = firebase.functions().httpsCallable('calc');
		// calc([id, { ...answers }, userName, email]);
		calc([id, answers, userName, email]);
	}

	return error;
}

export { answers };
export { McqStore };
export { infoAboutTheTest };
export { clearStorage };
