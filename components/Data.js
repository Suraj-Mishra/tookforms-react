import { v4 as uuidv4 } from 'uuid';

class Data {
	constructor() {
		this.option = '';
		this.answer = 'wrong';
		this.description = '';
		this.key = uuidv4();
		this.id = uuidv4();
	}
}

export default Data;
