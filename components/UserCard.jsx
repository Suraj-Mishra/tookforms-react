import { useState } from 'react';

const UserCard = props => {
	const [marquee, setMarquee] = useState();

	let name = [];
	let email = [];
	let modifiedName;
	let modifiedEmail;

	props.listOfCandidates.forEach(item => {
		if (item.indexOf(' ') >= 0) {
			name.push(item);
		}
		if (item.indexOf('@') >= 0) {
			email.push(item);
		}
	});

	if (name[0].length >= 39) {
		let subString = name[0].substring(0, 30);
		modifiedName = subString + '...';
	}

	if (email[0].length >= 39) {
		let subString = email[0].substring(0, 30);
		modifiedEmail = subString + '...';
	}

	function marqueeComponent(string, className) {
		return (
			<span className={className ? `${className} marquee` : 'marquee'}>
				<span>{string}</span>
			</span>
		);
	}

	function mailTo() {
		window.location.href = `mailto: ${email[0]}`;
	}

	return (
		<>
			<div
				className="user-card-container"
				onMouseEnter={() => setMarquee(true)}
				onMouseLeave={() => setMarquee(false)}
				onClick={() => mailTo()}
			>
				{marquee === true && name[0].length >= 39 ? (
					marqueeComponent(name[0], 'user-card-name')
				) : (
					<span className="user-card-name">{name[0].length > 35 ? modifiedName : name}</span>
				)}
				{marquee === true && email[0].length >= 39 ? (
					marqueeComponent(email[0])
				) : (
					<span className="user-card-email">{email[0].length > 35 ? modifiedEmail : email}</span>
				)}
			</div>
		</>
	);
};

export default UserCard;
