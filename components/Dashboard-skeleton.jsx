const Loader = () => {
	return (
		<div className="content">
			<div className="container">
				<div className="page-title" style={{ marginBottom: 100 }}>
					Dashboard
				</div>
				<div className="skeleton-a7x7cwtgk4i"></div>
				<div className="skeleton-a7x7cwtgk4i"></div>
				<div className="skeleton-a7x7cwtgk4i"></div>
				<div className="skeleton-a7x7cwtgk4i"></div>
			</div>
		</div>
	);
};

export default Loader;
