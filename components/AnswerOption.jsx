import { useEffect, useState } from 'react';
import { answers } from '../Stores/McqStore';

const AnswerOption = props => {
	const [focus, setFocus] = useState(false);

	useEffect(() => {
		if (answers[props.page] && answers[props.page][2] === props.id) {
			setFocus(true);
		}
	}, []);

	function selected(selected, id) {
		props.getSelected(selected, id);
	}

	function description() {
		return <span className="test-description">{props.description}</span>;
	}

	return (
		<>
			<div
				className="test-mcq-options-container"
				onClick={() => {
					selected(props.option, props.id);
				}}
			>
				<div className={answers[props.page] === undefined ? null : focus ? 'selected' : 'unselected'}>
					<div className="test-mcq-option-area">
						{focus ? (
							<img alt="radio-button" src="/Radio-Enabled.svg" />
						) : (
							<img alt="radio-button" src="/Disabled.svg" />
						)}
						<div className="test-content">
							<h1 className="test-question">{props.option}</h1>
							{/* <br /> */}
							{props.description ? description() : null}
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default AnswerOption;
