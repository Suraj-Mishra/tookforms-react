import { useState } from 'react';

const Accordian = props => {
	const [open, setOpen] = useState(false);
	return (
		<div className="main-content" onClick={() => setOpen(!open)}>
			<button className="accordion">
				<span> {props.title} </span>
				<span>
					<img
						className={open ? 'dropdown-arrow rotate' : 'dropdown-arrow'}
						src="/dropdown_down.svg"
						alt="drop down icon"
					/>
				</span>
			</button>
			<div className={open ? 'open' : 'closed'}>
				<p>{props.content}</p>
			</div>
		</div>
	);
};

export default Accordian;
