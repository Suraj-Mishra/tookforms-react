const AddQuestionsSkeleton = () => {
	const option = () => (
		<>
			<div className="mcq-options-container" onBlur={() => props.getState(state)}>
				<div className="mcq-option">
					<div className="mcq-option-area">
						<input type="text" name="Option" />

						<select name="answer type" id="answer-type" className="answer-type-dropdown"></select>
						<div className="dropdown">
							<button className="dropbtn"></button>
						</div>
						<div className={'option-description smalldesc'}>
							<div className="description-input">
								<input type="text" name="Option {option}" />
							</div>
							<div className="remove-description">
								<button className="icon-button"></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);

	const componentArray = [1, 2, 3, 4].map((item, index) => <option key={index} />);

	return (
		<>
			<div className="content">
				<div className="container">
					<div className="card addQuestion-skeleton">
						<label htmlFor="title">
							<h2>Add Questions</h2>
							<div className="subtitle-row">
								<div className="subtitle">
									<h4>Add the questions the test takers need to answer</h4>
								</div>
								<div className="ques-number">
									<span className="ques-counter"></span>
								</div>
							</div>
						</label>
						<input type="text" id="title" name="test title" className="input-large" />
						{componentArray}
						<button className="button-add-options">Add Option</button>

						<div className="buttons-row">
							<div className="buttons-left">
								<button className="button-secondary">Back</button>
							</div>
							<div className="buttons-right">
								<button className="button-secondary-disabled">Save and Publish</button>
								<button className={'button-primary-disabled'}>Next</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default AddQuestionsSkeleton;
