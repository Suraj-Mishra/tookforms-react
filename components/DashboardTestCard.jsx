import { useState } from 'react';
import { useRouter } from 'next/router';

const DashboardTestCard = props => {
	const [marquee, setMarquee] = useState();

	const router = useRouter();

	function gotoTest(id) {
		router.push(`/test/passList/${id}`);
	}

	let modifiedString;
	let title = Object.keys(props.data)[0];
	if (title.length > 25) {
		let subString = title.substring(0, 20);
		modifiedString = subString + '...';
	}
	let time = Object.values(props.data)[0].infoAboutTheTest.time;

	function marqueeComponent(title) {
		return (
			<span className="marquee">
				<span>{title}</span>
			</span>
		);
	}

	return (
		<>
			<div
				className="test-item"
				onMouseEnter={() => setMarquee(true)}
				onMouseLeave={() => setMarquee(false)}
				onClick={() => gotoTest(props.id)}
			>
				{marquee === true && title.length > 20 ? (
					marqueeComponent(title)
				) : (
					<div className="test-title">{title.length > 20 ? modifiedString : title}</div>
				)}
				<div className="date-status">
					<div className="date">{time}</div>
				</div>
			</div>
		</>
	);
};

export default DashboardTestCard;
