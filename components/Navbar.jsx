import { useRouter } from 'next/router';
import Link from 'next/link';
import Cookies from 'js-cookie';
import { useEffect, useState } from 'react';
import firebase from 'firebase/app';
import initFirebase from '../utils/auth/initFirebase';
import { clearStorage } from '../Stores/McqStore';
// import { useAuth } from '../utils/auth/auth';

initFirebase();

const Navbar = () => {
	// const { user } = useAuth();
	const router = useRouter();
	const pathname = router.pathname;

	const [name, setName] = useState('');
	const [dashboard, setDashboard] = useState();
	const [dropDown, setDropDown] = useState(false);

	function logOut() {
		setName('');
		firebase
			.auth()
			.signOut()
			.then(() => {
				Cookies.remove('user', { path: '/' });
				Cookies.remove('token', { path: '/' });
				router.push('/');
			})
			.catch(error => {
				console.log(error);
			});
	}

	useEffect(() => {
		if (pathname !== '/') {
			let user;
			let token;
			const userCookieData = Cookies.get('user');
			token = Cookies.get('token');
			if (userCookieData) user = JSON.parse(userCookieData);
			if (user && token && user !== '' && token !== '') {
				setName('Welcome, ' + user.displayName);
				if (!pathname.includes('test')) {
					setDashboard('Dashboard');
				}
				if (pathname.includes('/test/edit')) {
					setDashboard('Dashboard');
				}
				if (pathname.includes('/test/passList')) {
					setDashboard('Dashboard');
				}
			}
		} else {
			setName(null);
			setDashboard(null);
		}
	});

	return (
		<nav>
			<div className="navigation-container">
				<div className="logo">tookforms</div>
				<div className={name !== '' ? 'navigation-menu-items' : 'hide navigation-menu-items'}>
					<Link href="/dashboard">
						<a
							className={
								router.pathname === '/dashboard'
									? 'active navigationbar-dashboard'
									: 'inactive navigationbar-dashboard'
							}
							onClick={() => {
								clearStorage();
							}}
						>
							{dashboard}
						</a>
					</Link>
					<div className="dropdown-container" onBlur={() => setDropDown(false)}>
						<button onClick={() => setDropDown(!dropDown)} className="name-dropbtn">
							{name}
						</button>
						<div className={dropDown ? 'show name-dropdown-content' : 'hide name-dropdown-content'}>
							<a
								onMouseDown={e => {
									e.preventDefault();
									logOut();
									setDropDown(!dropDown);
								}}
							>
								Logout
							</a>
						</div>
					</div>
				</div>
			</div>
		</nav>
	);
};

export default Navbar;
