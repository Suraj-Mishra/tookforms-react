import Loader from 'react-loader-spinner';

const Load = props => (
	<Loader
		className="loader"
		type="MutatingDots"
		color="#00BFFF"
		height={100}
		width={100}
		visible={props.visible}
		// visible={true}
		// timeout={3000} //3 secs
	/>
);

export default Load;
