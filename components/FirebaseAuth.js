import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import firebase from 'firebase/app';
import 'firebase/auth';
import initFirebase from '../utils/auth/initFirebase';
import 'firebase/database';

// Init the Firebase app.
initFirebase();

const firebaseAuthConfig = {
	signInFlow: 'popup',
	// Auth providers
	// https://github.com/firebase/firebaseui-web#configure-oauth-providers
	signInOptions: [
		// {
		// 	provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
		// 	requireDisplayName: false,
		// },
		{
			provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
			scopes: ['email', 'https://www.googleapis.com/auth/userinfo.profile'],
		},
	],
	// signInSuccessUrl: '/dashboard',
	credentialHelper: 'none',
	callbacks: {
		signInSuccessWithAuthResult: ({ user }, redirectUrl) => {
			return false;
		},
	},
};

const FirebaseAuth = () => {
	return (
		<div>
			<StyledFirebaseAuth uiConfig={firebaseAuthConfig} firebaseAuth={firebase.auth()} />
		</div>
	);
};

export default FirebaseAuth;
