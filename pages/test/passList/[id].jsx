import { useRouter } from 'next/router';
import firebase from 'firebase/app';
// import 'firebase/firestore';
import { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { useEffect } from 'react';
import Load from '../../../components/Load';
import UserCard from '../../../components/UserCard';
import totalCardsData from '../../../Stores/IndexdDBFunctions';
import { parseCookies } from 'nookies';
// import { verifyIdToken } from '../../../utils/auth/firebaseAdmin';

const PassList = () => {
	const router = useRouter();
	// const { id } = router.query;
	const path = router.asPath;
	const id = path.substr(15);
	console.log(path);

	let userCard;
	let headString;
	const [title, setTitle] = useState('');
	const [dataPass, setDataPass] = useState();
	const [dataFail, setDataFail] = useState();
	const [dataAttempt, setDataAttempt] = useState();
	const [visible, setVisible] = useState(true);

	useEffect(() => {
		console.log(id !== '[id]');
		if (id) {
			let cookieData;
			const cookies = parseCookies();
			const cookieLength = Object.keys(cookies).length;
			if (cookieLength > 0) {
				cookieData = JSON.parse(cookies.user);
			}
			let modifiedEmail = cookieData.email.replace(/\./g, ',');
			firebase
				.firestore()
				.collection('testResult')
				.doc(id)
				.get()
				.then(snapShot => {
					if (snapShot.data()) {
						let result = snapShot.data();
						something1([result, 'pass']);
					} else {
						setVisible(false);
					}
				})
				.catch(err => console.log(err));
			firebase
				.firestore()
				.collection('failedResult')
				.doc(id)
				.get()
				.then(snapShot => {
					if (snapShot.data()) {
						let result = snapShot.data();
						something1([result, 'failed']);
					} else {
						setVisible(false);
					}
				})
				.catch(err => console.log(err));
			totalCardsData(modifiedEmail).iterate((value, key) => {
				if (key === id) {
					setTitle(Object.keys(value)[0]);
				}
			});
		}
	}, [id]);

	function something1(list) {
		let result = list[0].data.map(Object.values);
		userCard = result.map(item => <UserCard key={uuidv4()} listOfCandidates={item} />);
		if (list[1] === 'pass') {
			setDataPass(userCard);
		} else {
			setDataFail(userCard);
		}
		setDataAttempt(userCard);
		setVisible(false);
	}

	if (dataAttempt && dataAttempt.length > 1) {
		headString = `${dataAttempt.length} people appeared for this test`;
	} else if (dataAttempt) {
		headString = `${dataAttempt.length} person appeared for this test`;
	}

	return (
		<>
			<div className="passlist-head">
				<div>
					<div className="page-title">{title}</div>
					<div className="page-subtitle">{headString}</div>
				</div>
				<div>
					<button className="button-secondary" onClick={() => router.push(`/test/edit/${id}`)}>
						Edit form
					</button>
				</div>
			</div>
			<div className="passlist-container">
				{!dataAttempt && !visible ? (
					<div className="no-attempts">No candidate has appeared for this test.</div>
				) : (
					<div className="passlist-grid">
						<div className="passlist-section1 passlist-common">
							<span>{dataPass ? `Pass (${dataPass.length})` : ''}</span>
							<div className="passlist-pass passlist">{dataPass}</div>
						</div>
						<div className="passlist-section2 passlist-common">
							<span>{dataFail ? `Fail (${dataFail.length})` : ''}</span>
							<div className="passlist-fail passlist">{dataFail}</div>
						</div>
						<div className="passlist-section3 passlist-common">
							<span>{dataAttempt ? `Attempted (${dataAttempt.length})` : ''}</span>
							<div className="passlist-fail passlist">{dataAttempt}</div>
						</div>
					</div>
				)}
			</div>
			<Load visible={visible} />
		</>
	);
};

export default PassList;

// export async function getServerSideProps(ctx) {
// 	let uid, email;
// 	const cookies = nookies.get(ctx);
// 	if (cookies.token && cookies.token !== '') {
// 		const token = await verifyIdToken(cookies.token);
// 		uid = token.uid;
// 		email = token.email;
// 	}
// 	if (!uid && !email) {
// 		return {
// 			redirect: {
// 				permanent: false,
// 				destination: '/',
// 			},
// 		};
// 	}
// 	return {
// 		props: {},
// 	};
// }
