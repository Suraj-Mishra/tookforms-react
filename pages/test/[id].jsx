import { useEffect } from 'react';
import Accordian from '../../components/Accordian';
import FirebaseAuth from '../../components/FirebaseAuth';
import { useRouter } from 'next/router';
// import Cookies from 'js-cookie';
import nookies from 'nookies';
import firebase from 'firebase/app';
import { verifyIdToken } from '../../utils/auth/firebaseAdmin';
import { useAuth } from '../../utils/auth/auth';

const test = () => {
	const { user } = useAuth();

	const router = useRouter();
	const { id } = router.query;
	useEffect(() => {
		localStorage.setItem('id', id);
	});

	useEffect(() => {
		const unsubscribe = firebase.auth().onAuthStateChanged(user => {
			if (user) {
				router.push(`/test/questions/${id}`);
			}
		});
		unsubscribe();
	});

	const data = [
		{
			title: 'How does this test work?',
			content:
				'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
		},
		{
			title: 'Can I change my answer after submitting the test?',
			content:
				'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
		},
		{
			title: 'Can I appear for the test more than once?',
			content:
				'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
		},
	];

	const accordian = data.map((item, index) => <Accordian title={item.title} content={item.content} key={index} />);

	return (
		<>
			<section className="intro-container">
				<div className="intro-content">
					<h1>Coditas - Online test for UX design</h1>
					<p>This test is to get an idea of your understanding of UX design and its fundamentals.</p>
					{accordian}
					<FirebaseAuth />
				</div>
			</section>
		</>
	);
};

export default test;

export async function getServerSideProps(ctx) {
	const parameterId = ctx.params.id;
	let url = `/test/questions/${parameterId}`;
	let uid, email;
	nookies.destroy(undefined, 'token', { path: '/' });
	const cookies = nookies.get(ctx);
	if (cookies.token) {
		const token = await verifyIdToken(cookies.token);
		uid = token.uid;
		email = token.email;
	}
	if (uid && email) {
		return {
			redirect: {
				permanent: false,
				destination: url,
			},
		};
	}
	return {
		props: {},
	};
}
