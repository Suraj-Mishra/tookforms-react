import { useRouter } from 'next/router';
import { parseCookies } from 'nookies';
import { useEffect, useState } from 'react';
// import cookies from 'js-cookie';
import firebase from 'firebase/app';
import Options from './Options';
import Data from '../../../components/Data';
import { updateDataOnDatabase } from '../../../Stores/McqStore';
import AddQuestionsSkeleton from '../../../components/AddQuestions-skeleton';
import { DateTime } from 'luxon';
import TextareaAutosize from 'react-textarea-autosize';
// import { totalCardsData } from '../../../Stores/IndexdDBFunctions';
// import { verifyIdToken } from '../../../utils/auth/firebaseAdmin';
import { useAuth } from '../../../utils/auth/auth';
import totalCardsData from '../../../Stores/IndexdDBFunctions';

const edit = () => {
	const { user } = useAuth();

	const [data, setData] = useState();
	const [page, changePage] = useState(1);
	const [question, setQuestion] = useState('');
	const [disableButton, setDisableButton] = useState(true);
	const [addOption, setAddOption] = useState(false);
	const [totalPages, setTotalPages] = useState(1);
	const [forceUpdate, setForceUpdate] = useState(true);
	const [info, setInfo] = useState();
	const [visible, setVisible] = useState(false);
	const [count, setCount] = useState(false);

	const router = useRouter();
	// const path = router.asPath;
	// const id = path.substr(11);
	const path = router.asPath;
	const id = path.substr(11);
	console.log(id);

	function addOptions() {
		data[page].options.push(new Data());
		setAddOption(!addOption);
	}

	useEffect(() => {
		let cookieData, email;
		if (id !== '[id]') {
			const cookies = parseCookies();
			const cookieLength = Object.keys(cookies).length;
			if (cookieLength > 0) {
				cookieData = JSON.parse(cookies.user);
				email = cookieData.email;
			} else {
				router.push('/');
			}
			let values = [];
			totalCardsData(email)
				.iterate((value, key, iterationNumber) => {
					if (key === id) {
						values.push(value);
					}
				})
				.then(() => {
					if (values.length === 0) {
						console.log(id);
						let modifiedEmail = email.replace(/\./g, ',');
						console.log(modifiedEmail);
						firebase
							.database()
							.ref(`/${modifiedEmail}/${id}`)
							.once('value')
							.then(snapshot => {
								let data = snapshot.val();
								let keys = Object.values(data);
								let McqResult = keys[0].McqStore;
								let infoAboutTheTest = keys[0].infoAboutTheTest;
								setInfo(infoAboutTheTest);
								McqResult.unshift({});
								setData(McqResult);
								setQuestion(McqResult[page].question);
								setTotalPages(McqResult.length - 1);
							});
					} else {
						let keys = Object.values(values[0]);
						let McqResult = keys[0].McqStore;
						let infoAboutTheTest = keys[0].infoAboutTheTest;
						setInfo(infoAboutTheTest);
						if (Object.entries(McqResult[0]).length !== 0) {
							McqResult.unshift({});
						}
						setData(McqResult);
						setQuestion(McqResult[page].question);
						setTotalPages(McqResult.length - 1);
					}
				});
		}
	}, [id]);

	useEffect(() => {
		if (data) {
			let options = data[page].options.map(item => {
				return item.option;
			});
			let answers = data[page].options.map(item => {
				return item.answer;
			});
			let answersFiltered = answers.filter(item => item !== 'wrong');
			let optionsFiltered = options.filter(item => item !== '');
			if (question.length > 1 && optionsFiltered.length > 0 && answersFiltered.length > 0) {
				data[page].question = question;
				setDisableButton(false);
			} else {
				data[page].question = '';
				setDisableButton(true);
			}
		}
	}, [count, question]);

	function getState(state) {
		data[page].options.forEach((element, index) => {
			if (element.id === state.id) {
				data[page].options[index] = state;
			}
		});
		setCount(!count);
		console.count('count');
	}

	function goBack() {
		if (page !== 1) {
			changePage(page - 1);
		}
	}

	function next() {
		if (data.length !== page + 1) {
			changePage(page + 1);
		}
	}

	useEffect(() => {
		if (data) {
			setQuestion(data[page].question);
		}
	}, [page]);

	function updateData() {
		let infoAboutTheTest = [];
		let McqStore = [];
		infoAboutTheTest.push(info);
		McqStore.push(data);
		infoAboutTheTest = info;
		McqStore = data;
		let dt = DateTime.local().toLocaleString();
		infoAboutTheTest.time = dt;
		let wholeData = {
			[infoAboutTheTest.title]: {
				infoAboutTheTest,
				McqStore,
			},
		};
		updateDataOnDatabase(id, wholeData);

		router.push('/testLink');
	}

	let componentArray;

	if (data && Object.entries(data).length !== 0) {
		console.log(data[page]);
		console.log(data);
		componentArray = data[page].options.map((item, index) => (
			<Options
				key={item.key}
				keys={item.key}
				id={item.id}
				getState={getState}
				page={page}
				option={item.option}
				answer={item.answer}
				description={item.description}
				forceUpdate={forceUpdate}
				setForceUpdate={setForceUpdate}
				data={data}
				index={index + 1}
				setVisible={setVisible}
				visible={visible}
			/>
		));
	}

	return !data ? (
		<AddQuestionsSkeleton />
	) : (
		<>
			<div className="content">
				<div className="container">
					<div className="card">
						<label htmlFor="title">
							<h2>Add Questions</h2>
							<div className="subtitle-row">
								<div className="subtitle">
									<h4>Add the questions the test takers need to answer</h4>
								</div>
								<div className="ques-number">
									<span className="ques-counter">
										{page}/{totalPages}
									</span>
								</div>
							</div>
						</label>
						<TextareaAutosize
							id="title"
							className="title-textarea"
							onChange={e => {
								const { value } = e.target;
								setQuestion(value);
							}}
							value={question}
							maxRows={8}
							minRows={1}
						/>
						{componentArray}
						<button className="button-add-options" onClick={() => addOptions()}>
							Add Option
						</button>

						<div className="buttons-row">
							{page !== 1 ? (
								<div className="buttons-left">
									<button className="button-secondary" onClick={() => goBack()}>
										Back
									</button>
								</div>
							) : null}
							<div className="buttons-right">
								<button
									className={disableButton ? 'button-secondary-disabled' : 'button-secondary'}
									onClick={() => updateData()}
									disabled={disableButton ? true : false}
								>
									Save and Publish
								</button>
								<button
									className={
										disableButton
											? 'button-primary-disabled'
											: page === totalPages
											? 'button-primary-disabled'
											: 'button-primary-enabled'
									}
									disabled={disableButton ? true : false}
									onClick={() => {
										next();
									}}
								>
									Next Question
								</button>
							</div>
						</div>
					</div>
				</div>
				<div className={visible ? 'up removed-option-toast' : 'down removed-option-toast'}>
					Option removed <span>Undo</span>
				</div>
			</div>
		</>
	);
};

export default edit;

// export async function getServerSideProps(ctx) {
// 	try {
// 		const cookies = nookies.get(ctx);
// 		const token = await verifyIdToken(cookies.token);
// 		const { uid, email } = token;
// 		if (!uid && !email) {
// 			return {
// 				redirect: {
// 					permanent: false,
// 					destination: '/',
// 				},
// 			};
// 		}
// 	} catch (error) {
// 		return {
// 			props: {},
// 		};
// 	}

// 	return {
// 		props: {},
// 	};
// }
