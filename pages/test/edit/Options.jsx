import { useState, useEffect } from 'react';

const Options = props => {
	const [state, setState] = useState({
		option: '',
		answer: 'wrong',
		description: '',
		id: props.id,
		key: props.keys,
	});
	const [slideDown, setSlideDown] = useState(true);
	const [dropDown, setDropDown] = useState(false);
	const [optionDropDown, setOptionDropDown] = useState(false);

	function removeOption() {
		props.data[props.page].options.forEach((element, index) => {
			if (element.id === state.id) {
				props.data[props.page].options.splice(index, 1);
			}
		});
		props.setForceUpdate(!props.forceUpdate);
		props.setVisible(true);
		window.setTimeout(() => props.setVisible(false), 2000);
	}

	useEffect(() => {
		props.data[props.page].options.forEach(element => {
			if (state.id === element.id) {
				setState(prevState => ({
					...prevState,
					option: element.option,
					answer: element.answer,
					description: element.description,
				}));
			}
		});
	}, [props.page]);

	useEffect(() => {
		props.getState(state);
	}, [state]);

	return (
		<>
			<div
				className="mcq-options-container"
				// onBlur={() => props.getState(state)}
			>
				<div className="mcq-option">
					<div className="mcq-option-area">
						<input
							type="text"
							name="Option"
							placeholder={'Option ' + props.index}
							onInput={e => {
								const { value } = e.target;
								setState(prevState => ({ ...prevState, option: value }));
							}}
							value={state.option}
						/>
						<div
							tabIndex={0}
							onBlur={() => setOptionDropDown(false)}
							className={'dropdown dropdown-wrapper ' + state.answer}
							onClick={() => setOptionDropDown(!optionDropDown)}
						>
							<div className="dropdown-div">
								<div>{state.answer.charAt(0).toUpperCase() + state.answer.slice(1)} Answer</div>
								<img src={'/ic_arrow_down_dropdown.svg'} alt="expand" />
							</div>
							<div
								id="myDropdown"
								className={optionDropDown ? 'show dropdown-content' : 'dropdown-content'}
							>
								<a
									onClick={e => {
										const { innerText } = e.target;
										setState(prevState => ({ ...prevState, answer: 'correct' }));
										setOptionDropDown(!optionDropDown);
									}}
									className="correct"
								>
									Correct Answer
								</a>
								<a
									onClick={e => {
										const { innerText } = e.target;
										setState(prevState => ({ ...prevState, answer: 'wrong' }));
										setOptionDropDown(!optionDropDown);
									}}
									className="wrong"
								>
									Wrong Answer
								</a>
								{/* <a
									onClick={e => {
										const { innerText } = e.target;
										setState(prevState => ({ ...prevState, answer: 'neutral' }));
										setOptionDropDown(!optionDropDown);
									}}
									className="neutral"
								>
									Neutral Answer
								</a> */}
							</div>
						</div>

						<div className="dropdown" onBlur={() => setDropDown(false)}>
							<button className="dropbtn" onClick={() => setDropDown(!dropDown)}></button>
							<div id="myDropdown" className={dropDown ? 'show dropdown-content' : 'dropdown-content'}>
								<a
									onMouseDown={e => {
										e.preventDefault();
										setSlideDown(!slideDown);
										setDropDown(!dropDown);
									}}
								>
									Add Description
								</a>
								<a
									onMouseDown={e => {
										e.preventDefault();
										removeOption();
									}}
								>
									Remove Option
								</a>
							</div>
						</div>
						<div
							className={
								slideDown
									? 'option-description smalldesc'
									: 'option-description smalldesc expand-smalldesc'
							}
						>
							<div className="description-input">
								<input
									type="text"
									name="Option {option}"
									placeholder="Write description"
									value={state.description}
									onInput={e => {
										const { value } = e.target;
										setState(prevState => ({ ...prevState, description: value }));
									}}
								/>
							</div>
							<div className="remove-description">
								<button
									className="icon-button"
									onClick={() => {
										setSlideDown(!slideDown);
										setState(prevState => ({ ...prevState, description: '' }));
									}}
								></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default Options;
