import { useEffect, useState } from 'react';
import AnswerOption from '../../../components/AnswerOption';
import { answers, storeCandidateTestAnswers } from '../../../Stores/McqStore';
import Data from '../../../components/Data';
import 'firebase/functions';
import { useRouter } from 'next/router';
import firebase from 'firebase/app';
import nookies, { destroyCookie } from 'nookies';
import { verifyIdToken } from '../../../utils/auth/firebaseAdmin';
// import { useAuth } from '../../../utils/auth/auth';

const questions = ({ product }) => {
	// const { user } = useAuth();

	const [page, changePage] = useState(0);
	const [defaultOption, setDefaultOption] = useState();
	const [selected, setSelected] = useState();
	const [count, setCount] = useState(0);
	const [disableButton, setDisableButton] = useState(true);

	const router = useRouter();
	const { id } = router.query;

	let question;
	useEffect(() => {
		localStorage.removeItem('id');
		localStorage.removeItem('testKey');
	}, []);

	useEffect(() => {
		if (answers[page]) {
			setDefaultOption(answers[page][2]);
		}
		setSelected(null);
	}, [page]);

	function getSelected(val, selectedIndex) {
		if (answers.length === 0 || answers[page] === undefined) {
			answers.push([question, val, selectedIndex]);
		} else {
			answers.forEach((item, index) => {
				if (question === item[0]) {
					answers[page] = [question, val, selectedIndex];
				}
			});
		}
		setDefaultOption(null);

		setSelected(selectedIndex);

		setDisableButton(false);
	}

	const result = product[0][page].map((item, index) => {
		let value;
		let option;
		let description;
		if (index === 0) {
			question = item;
		} else {
			if (index !== 0) {
				value = item.map((item, index) => {
					let id = new Data();
					[option, description] = item;
					return (
						<AnswerOption
							key={id.key}
							id={index}
							option={option}
							description={description}
							getSelected={getSelected}
							// defaultOption={defaultOption}
							// selected={selected}
							page={page}
						/>
					);
				});
				return value;
			}
		}
	});

	async function next() {
		if (disableButton === false) {
			if (product[0].length - 1 !== page) {
				changePage(page + 1);
			} else {
				if (count === 0) {
					firebase.auth().onAuthStateChanged(function (user) {
						if (user) {
							const data = { id, answers, displayName: user.displayName, email: user.email };
							localStorage.setItem('data', JSON.stringify(data));
							router.push('/testComplete');
						}
					});
					setCount(1);
				}
			}
		}
	}

	function goBack() {
		if (page > 0) {
			changePage(page - 1);
		}
	}

	useEffect(() => {
		if (answers[page] !== undefined) {
			setDisableButton(false);
		} else {
			setDisableButton(true);
		}
	}, [page]);

	return (
		<>
			<div className="content">
				<div className="test-container">
					<div></div>
					<div className="card-background-change">
						<label htmlFor="title">
							<h2>{question}</h2>
						</label>
						{result}
						<div className="buttons-row">
							{page !== 0 ? (
								<div className="buttons-left">
									<button className="button-secondary" onClick={() => goBack()}>
										Back
									</button>
								</div>
							) : null}
							<div className="buttons-right next-button-section">
								<div
									className={disableButton ? 'next-question-btn' : 'enabled next-question-btn'}
									onClick={() => {
										next();
									}}
								>
									Save and continue{' '}
									<div
										className={
											disableButton
												? 'next-question-btn-div'
												: 'enabled-div next-question-btn-div'
										}
									>
										{page + 1}/{product[0].length}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default questions;

export async function getServerSideProps(ctx) {
	const parameterId = ctx.params.id;
	let uid, email;
	const cookies = nookies.get(ctx);
	if (cookies.token && cookies.token !== '') {
		const token = await verifyIdToken(cookies.token);
		uid = token.uid;
		email = token.email;
	}
	if (!uid && !email) {
		return {
			redirect: {
				permanent: false,
				destination: '/',
			},
		};
	}
	const res = await fetch(`https://tookforms-react.vercel.app/api/tests/${parameterId}`);
	const data = await res.json();
	return {
		props: { product: data },
	};
}
