import { parseCookies } from 'nookies';
import { storeCandidateTestAnswers } from '../Stores/McqStore';
import { useState } from 'react';
import { useRouter } from 'next/router';

// import { verifyIdToken } from '../utils/auth/firebaseAdmin';

const testComplete = () => {
	const [visible, setVisible] = useState(false);
	const [count, setCount] = useState(0);

	const router = useRouter();

	async function sendData() {
		const cookies = parseCookies();
		const cookieLength = Object.keys(cookies).length;
		if (count === 0 && cookieLength > 0) {
			setCount(count + 1);
			let data = localStorage.getItem('data');
			let parsedData = JSON.parse(data);
			let error = storeCandidateTestAnswers(
				parsedData.id,
				parsedData.answers,
				parsedData.displayName,
				parsedData.email,
			);
			let answer = await error;
			if (!answer) {
				setVisible(true);
				window.setTimeout(() => setVisible(false), 2000);
			}
		} else {
			router.push('/');
		}
	}

	return (
		<>
			<section className="testComplete-container">
				<div className="testComplete-content">
					<h1>Test Completed</h1>
					<p>
						You have successfully completed our test. We will evaluate your response and reach out to you
						very soon.
					</p>
					<p>Please click on submit response button to submit your answers</p>
					<button onClick={() => sendData()} className="submit-response-button">
						Submit Response
					</button>
				</div>
				<div className={visible ? 'up link-copied-toast' : 'down link-copied-toast'}>
					Test Response Successfully Submitted
				</div>
			</section>
		</>
	);
};

export default testComplete;

// export async function getServerSideProps(ctx) {
// 	let uid, email;
// 	const cookies = nookies.get(ctx);
// 	if (cookies.token && cookies.token !== '') {
// 		const token = await verifyIdToken(cookies.token);
// 		uid = token.uid;
// 		email = token.email;
// 	}
// 	if (!uid && !email) {
// 		return {
// 			redirect: {
// 				permanent: false,
// 				destination: '/',
// 			},
// 		};
// 	}
// 	return {
// 		props: {},
// 	};
// }
