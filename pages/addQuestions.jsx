import { useState, useEffect } from 'react';
import Options from '../components/Options';
import { McqStore, sendDataToFirebase } from '../Stores/McqStore';
import Data from '../components/Data';
import { infoAboutTheTest } from '../Stores/McqStore';
import { DateTime } from 'luxon';
// import nookies from 'nookies';
// import 'firebase/functions';
import { useRouter } from 'next/router';
import TextareaAutosize from 'react-textarea-autosize';
import Load from '../components/Load';
// import { verifyIdToken } from '../utils/auth/firebaseAdmin';
import { useAuth } from '../utils/auth/auth';
import firebase from 'firebase/app';

const AddQuestions = () => {
	const { user } = useAuth();

	const [question, setQuestion] = useState('');
	const [page, changePage] = useState(1);
	const [disableButton, setDisableButton] = useState(true);
	const [addOption, setAddOption] = useState(false);
	const [totalPages, setTotalPages] = useState(1);
	const [forceUpdate, setForceUpdate] = useState(true);
	const [enable, setEnable] = useState(false);
	const [visible, setVisible] = useState(false);
	const [count, setCount] = useState(false);

	useEffect(() => {
		let title = localStorage.getItem('title');
		let score = localStorage.getItem('score');
		let description = localStorage.getItem('description');
		infoAboutTheTest.title = title;
		infoAboutTheTest.score = score;
		infoAboutTheTest.description = description;
		console.log(infoAboutTheTest);
	}, []);

	const router = useRouter();

	async function publishData() {
		let dt = DateTime.local().toLocaleString();
		infoAboutTheTest.time = dt;
		firebase.auth().onAuthStateChanged(user => {
			if (user) {
				sendDataToFirebase(changePage);
				router.push('/testLink');
			} else {
				router.push('/');
			}
		});
	}

	function addOptions() {
		McqStore[page].options.push(new Data());
		setAddOption(!addOption);
	}

	function getState(state) {
		McqStore[page].options.forEach((element, index) => {
			if (element.id === state.id) {
				McqStore[page].options[index] = state;
			}
		});
		setCount(!count);
	}

	useEffect(() => {
		let options = McqStore[page].options.map(item => {
			return item.option;
		});

		let answers = McqStore[page].options.map(item => {
			return item.answer;
		});

		let answersFiltered = answers.filter(item => item !== 'wrong');
		let optionsFiltered = options.filter(item => item !== '');

		if (optionsFiltered.length > 0 && question.length > 1 && answersFiltered.length > 0) {
			setDisableButton(false);
			McqStore[page].question = question;
		} else {
			setDisableButton(true);
			McqStore[page].question = '';
		}
	}, [count, question]);

	useEffect(() => {
		McqStore[page].question = question;
	}, [question]);

	useEffect(() => {
		setQuestion(McqStore[page].question);
	}, [page]);

	function next() {
		if (page === totalPages) {
			setTotalPages(totalPages + 1);
		}
		changePage(page + 1);
		if (McqStore.length === page + 1) {
			McqStore.push({ question: '', options: [new Data(), new Data(), new Data(), new Data()] });
		}
	}

	function goBack() {
		if (page !== 1) {
			changePage(page - 1);
		}
	}

	useEffect(() => {
		setQuestion(McqStore[page].question);
	}, [page]);

	if (McqStore.length === 0) {
		McqStore.push({});
		McqStore.push({ question: '', options: [new Data(), new Data(), new Data(), new Data()] });
	}

	let componentArray = [];

	if (McqStore.length > 1 && Object.keys(McqStore[0]).length === 0) {
		componentArray = McqStore[page].options.map((item, index) => (
			<Options
				key={item.key}
				keys={item.key} //We  pass the keys forward to the state
				id={item.id}
				getState={getState}
				page={page}
				option={item.option}
				answer={item.answer}
				description={item.description}
				forceUpdate={forceUpdate}
				setForceUpdate={setForceUpdate}
				index={index + 1}
				setEnable={setEnable}
			/>
		));
	}

	return (
		<>
			<div className="content">
				<div className="card-container">
					<div className="card">
						<label htmlFor="title">
							<h2>Add Questions</h2>
							<div className="subtitle-row">
								<div className="subtitle">
									<h4>Add the questions the test takers need to answer</h4>
								</div>
								<div className="ques-number">
									<span className="ques-counter">
										{page}/{totalPages}
									</span>
								</div>
							</div>
						</label>
						<TextareaAutosize
							id="title"
							className="title-textarea"
							onChange={e => {
								const { value } = e.target;
								setQuestion(value);
							}}
							value={question}
							maxRows={8}
							minRows={1}
						/>
						{componentArray}
						<button className="button-add-options" onClick={() => addOptions()}>
							Add Option
						</button>

						<div className="buttons-row">
							{page !== 1 ? (
								<div className="buttons-left">
									<button className="button-secondary" onClick={() => goBack()}>
										Back
									</button>
								</div>
							) : null}
							<div className="buttons-right">
								<button
									className={disableButton ? 'button-secondary-disabled' : 'button-secondary'}
									onClick={() => {
										setVisible(true);
										publishData();
									}}
									disabled={disableButton ? true : false}
								>
									Save and Publish
								</button>
								<button
									className={disableButton ? 'button-primary-disabled' : 'button-primary-enabled'}
									disabled={disableButton ? true : false}
									onClick={() => {
										next();
									}}
								>
									{page === totalPages ? 'Add Question' : 'Next Question'}
								</button>
							</div>
						</div>
					</div>
				</div>
				<div className={enable ? 'up removed-option-toast' : 'down removed-option-toast'}>
					{/* Option removed <span>Undo</span> */}
					Option removed
				</div>
				<Load visible={visible} />
			</div>
		</>
	);
};

// export async function getServerSideProps(ctx) {
// 	let uid, email;
// 	const cookies = nookies.get(ctx);
// 	if (cookies.token) {
// 		const token = await verifyIdToken(cookies.token);
// 		uid = token.uid;
// 		email = token.email;
// 	}
// 	if (!uid && !email) {
// 		return {
// 			redirect: {
// 				permanent: false,
// 				destination: '/',
// 			},
// 		};
// 	}
// 	return {
// 		props: {},
// 	};
// }

export default AddQuestions;
