import { useState, useEffect } from 'react';
import Link from 'next/link';
// import nookies from 'nookies';
import { useRouter } from 'next/router';
// import { verifyIdToken } from '../utils/auth/firebaseAdmin';

const testLink = () => {
	const [query, setQuery] = useState();
	const [visible, setVisible] = useState(false);

	const router = useRouter();

	useEffect(() => {
		let key = localStorage.getItem('testKey');
		if (key) {
			// setQuery(`http://localhost:3000/test/${key}`);
			setQuery(`https://tookforms-react.vercel.app/test/${key}`);
		}
	}, []);

	function copyToClipBoard() {
		navigator.clipboard.writeText(query).then(() => setVisible(true));
		window.setTimeout(() => setVisible(false), 2000);
	}

	function goToEdit() {
		let key = localStorage.getItem('testKey');
		router.push(`/test/edit/${key}`);
	}

	return (
		<>
			<div className="content">
				<div className="link-container">
					<div className="link-card">
						<h2>Ready to go!</h2>
						<h4>Just copy and share the test URL shown below with the test takers.</h4>

						<div className="link-section">
							<h3>UX Design entrance test</h3>
							<span>{query}</span>
						</div>

						<div className="link-buttons-row">
							<Link href="/dashboard">
								<div className="buttons-left">
									<button className="button-secondary">Back To Dashboard</button>
								</div>
							</Link>
							<div className="buttons-right">
								<button className="button-secondary" onClick={() => goToEdit()}>
									Edit Form
								</button>
								<button className="copy-link" onClick={() => copyToClipBoard()}>
									Copy Test URL to Clipboard
								</button>
							</div>
						</div>
					</div>
					<div className={visible ? 'up link-copied-toast' : 'down link-copied-toast'}>
						Test URL Copied to Clipboard
					</div>
				</div>
			</div>
		</>
	);
};

export default testLink;

// export async function getServerSideProps(ctx) {
// 	let uid, email;
// 	const cookies = nookies.get(ctx);
// 	if (cookies.token && cookies.token !== '') {
// 		const token = await verifyIdToken(cookies.token);
// 		uid = token.uid;
// 		email = token.email;
// 	}
// 	if (!uid && !email) {
// 		return {
// 			redirect: {
// 				permanent: false,
// 				destination: '/',
// 			},
// 		};
// 	}
// 	return {
// 		props: {},
// 	};
// }
