// import nookies from 'nookies';
// import { verifyIdToken } from '../utils/auth/firebaseAdmin';

const denyAccess = () => {
	return (
		<div className="gif-wrapper">
			<img src="/tenor.gif" />
			<h1>You can't attempt same test twice.</h1>
		</div>
	);
};

export default denyAccess;

// export async function getServerSideProps(ctx) {
// 	let uid, email;
// 	const cookies = nookies.get(ctx);
// 	if (cookies.token && cookies.token !== '') {
// 		const token = await verifyIdToken(cookies.token);
// 		uid = token.uid;
// 		email = token.email;
// 	}
// 	if (!uid && !email) {
// 		return {
// 			redirect: {
// 				permanent: false,
// 				destination: '/',
// 			},
// 		};
// 	}
// 	return {
// 		props: {},
// 	};
// }
