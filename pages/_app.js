import '../style.css';
import Layout from '../components/Layout';
import { AuthProvider } from '../utils/auth/auth';

function MyApp({ Component, pageProps }) {
	return (
		<AuthProvider>
			<Layout>
				<Component {...pageProps} />
			</Layout>
		</AuthProvider>
	);
}

export default MyApp;
