import FirebaseAuth from '../components/FirebaseAuth';
import 'firebaseui/dist/firebaseui.css';
import { useEffect, useState } from 'react';
import nookies from 'nookies';
import { useRouter } from 'next/router';
import localforage from 'localforage';
import { verifyIdToken } from '../utils/auth/firebaseAdmin';
import firebase from 'firebase/app';

const Index = () => {
	const router = useRouter();

	useEffect(() => {
		localStorage.clear();
		localforage.config({
			driver: localforage.INDEXEDDB,
			name: 'TookForms',
			storeName: 'test data',
		});
	}, []);

	firebase.auth().onAuthStateChanged(user => {
		// useEffect(() => {
		if (user) {
			router.push('/dashboard');
		}
		// }, []);
	});

	return (
		<>
			<section>
				<div className="intro-text">
					<div>
						<h1>Sign up</h1>
						<span>
							Welcome to tookforms <br></br>Please sign up to continue creating your test
						</span>
						<FirebaseAuth />
					</div>
				</div>
			</section>
		</>
	);
};

export async function getServerSideProps(ctx) {
	const cookies = nookies.get(ctx);
	if (cookies.token && cookies.token !== '') {
		const token = await verifyIdToken(cookies.token);
		const { uid, email } = token;
		if (uid && email) {
			return {
				redirect: {
					permanent: false,
					destination: '/dashboard',
				},
			};
		}
	}

	return {
		props: {},
	};
}

export default Index;
