import Link from 'next/link';
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
// import nookies from 'nookies';
import TextareaAutosize from 'react-textarea-autosize';
import { infoAboutTheTest, clearStorage } from '../Stores/McqStore';
import Load from '../components/Load';
// import { verifyIdToken } from '../utils/auth/firebaseAdmin';
import { useAuth } from '../utils/auth/auth';

const createNew = () => {
	const { user } = useAuth();

	const router = useRouter();

	const [disableButton, setDisableButton] = useState(false);
	const [title, setTitle] = useState('');
	const [score, setScore] = useState('');
	const [visible, setVisible] = useState(false);

	const gotoNextPage = () => {
		if (disableButton) {
			router.push('/createNewAdditionalInfo');
		}
	};

	useEffect(() => {
		// setTitle(infoAboutTheTest.title);
		// setScore(infoAboutTheTest.score);
		let titleFromStorage = localStorage.getItem('title');
		let scoreFromStorage = localStorage.getItem('score');
		if (titleFromStorage && scoreFromStorage) {
			setTitle(titleFromStorage);
			setScore(scoreFromStorage);
		}
	}, []);

	useEffect(() => {
		if (title !== '' && score !== '') {
			infoAboutTheTest.title = title;
			infoAboutTheTest.score = score;
			localStorage.setItem('title', title);
			localStorage.setItem('score', score);
			if (title !== '' && score !== '' && score <= 100 && score >= 0) {
				setDisableButton(true);
			} else {
				setDisableButton(false);
			}
		}
		console.log('2');
	});

	console.count('count');

	return (
		<>
			<div className="content">
				<div className="container">
					<div className="card">
						<h2>Create Test</h2>
						<h4>What would you call your test?</h4>
						<TextareaAutosize
							id="title"
							className="title-textarea"
							onChange={e => {
								const { value } = e.target;
								setTitle(value);
							}}
							value={title}
							maxRows={8}
							minRows={1}
						/>

						<div className="hint-yellow">
							💡
							<p>
								For example <b>“Microsoft - UX design enterance test”</b>. This will appear on the first
								screen the test takers are going to see. You can include your company’s name, and title
								of the project.
							</p>
						</div>
						<label htmlFor="input-normal">Passing Criteria (%)</label>
						<input
							name="test score"
							id="input-normal"
							className="input-normal"
							placeholder="Enter ‘0’ for no percentage criteria"
							type="number"
							max={100}
							min={0}
							onChange={e => setScore(e.target.value)}
							value={score}
						/>
						<div className="buttons-row">
							<div
								className="buttons-left"
								onClick={() => {
									clearStorage();
								}}
							>
								<Link href="/dashboard">
									<a>Discard Test</a>
								</Link>
							</div>
							<div className="buttons-right">
								<button
									disabled={disableButton ? false : true}
									className={disableButton ? 'button-primary-enabled' : 'button-primary-disabled'}
									onClick={() => {
										setVisible(true);
										gotoNextPage();
									}}
								>
									Next
								</button>
							</div>
						</div>
					</div>
				</div>
				<Load visible={visible} />
			</div>
		</>
	);
};

// export async function getServerSideProps(ctx) {
// 	let uid, email;
// 	const cookies = nookies.get(ctx);
// 	if (cookies.token) {
// 		const token = await verifyIdToken(cookies.token);
// 		uid = token.uid;
// 		email = token.email;
// 	}
// 	if (!uid && !email) {
// 		return {
// 			redirect: {
// 				permanent: false,
// 				destination: '/',
// 			},
// 		};
// 	}
// 	return {
// 		props: {},
// 	};
// }

export default createNew;
