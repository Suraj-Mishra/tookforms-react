import Link from 'next/link';
import { useRouter } from 'next/router';
import { useState, useEffect } from 'react';
// import nookies from 'nookies';
import TextareaAutosize from 'react-textarea-autosize';
import { infoAboutTheTest, clearStorage } from '../Stores/McqStore';
import Load from '../components/Load';
// import { verifyIdToken } from '../utils/auth/firebaseAdmin';
import { useAuth } from '../utils/auth/auth';

const createNewAdditionalInfo = () => {
	const { user } = useAuth();

	const router = useRouter();

	const [disableButton, setDisableButton] = useState(true);
	const [description, setDescription] = useState('');
	const [visible, setVisible] = useState(false);

	const gotoNextPage = () => {
		if (!disableButton) {
			router.push('/addQuestions');
		} else {
			return;
		}
	};

	useEffect(() => {
		setDescription(infoAboutTheTest.testDescription);
		let descriptionFromStorage = localStorage.getItem('description');
		if (descriptionFromStorage) {
			setDescription(descriptionFromStorage);
		}
	}, []);

	useEffect(() => {
		if (description !== '' && description !== undefined) {
			infoAboutTheTest.testDescription = description;
			localStorage.setItem('description', description);
			setDisableButton(false);
		} else {
			setDisableButton(true);
		}
	}, [description]);

	return (
		<>
			<div className="content">
				<div className="container">
					<div className="card">
						<h2>Create Test</h2>
						<h4>Add any additional details for the test takers</h4>

						<TextareaAutosize
							id="title"
							className="title-textarea"
							onChange={e => {
								const { value } = e.target;
								setDescription(value);
							}}
							value={description}
							maxRows={8}
							minRows={1}
						/>

						<div className="hint-yellow">
							💡
							<p>
								This description is to let the test takers know a few details about your test and any
								other relevant information.
							</p>
						</div>

						<div className="buttons-row">
							<div
								className="buttons-left"
								onClick={() => {
									clearStorage();
								}}
							>
								<Link href="/dashboard">
									<a>Discard Test</a>
								</Link>
							</div>
							<div className="buttons-right">
								<Link href="/createNew">
									<button className="button-secondary">Back</button>
								</Link>
								<button
									disabled={disableButton ? true : false}
									className={disableButton ? 'button-primary-disabled' : 'button-primary-enabled'}
									onClick={() => {
										setVisible(true);
										gotoNextPage();
									}}
								>
									Next
								</button>
							</div>
						</div>
					</div>
				</div>
				<Load visible={visible} />
			</div>
		</>
	);
};

// export async function getServerSideProps(ctx) {
// 	let uid, email;
// 	const cookies = nookies.get(ctx);
// 	if (cookies.token) {
// 		const token = await verifyIdToken(cookies.token);
// 		uid = token.uid;
// 		email = token.email;
// 	}
// 	if (!uid && !email) {
// 		return {
// 			redirect: {
// 				permanent: false,
// 				destination: '/',
// 			},
// 		};
// 	}
// 	return {
// 		props: {},
// 	};
// }

export default createNewAdditionalInfo;
