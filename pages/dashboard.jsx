import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import firebase from 'firebase/app';
import initFirebase from '../utils/auth/initFirebase';
import 'firebase/database';
import 'firebase/auth';
import Loader from '../components/Dashboard-skeleton';
import { parseCookies } from 'nookies';
import 'firebase/functions';
import { v4 as uuidv4 } from 'uuid';
import DashboardTestCard from '../components/DashboardTestCard';
import Load from '../components/Load';
// import { totalCardsData } from '../Stores/IndexdDBFunctions';
import totalCardsData from '../Stores/IndexdDBFunctions';

// import { verifyIdToken } from '../utils/auth/firebaseAdmin';
// import { useAuth } from '../utils/auth/auth';

initFirebase();

const dashboard = () => {
	// const { user } = useAuth();

	useEffect(() => {
		localStorage.clear();
	});

	const router = useRouter();
	const redirectToCreateTest = () => {
		router.push('/createNew');
	};

	let components = [];
	const [count, setCount] = useState(0);
	const [arr, setArr] = useState();
	const [paramId, setParamId] = useState(true);
	const [visible, setVisible] = useState(false);

	useEffect(() => {
		const id = localStorage.getItem('id');
		if (id) {
			router.push(`/test/questions/${id}`);
		} else {
			setParamId(false);
		}
	});

	function getEmail() {
		let cookieData;
		const cookies = parseCookies();
		const cookieLength = Object.keys(cookies).length;
		if (cookieLength > 0) {
			cookieData = JSON.parse(cookies.user);
		}
		if (!cookieData) {
			router.push('/');
		} else {
			return cookieData.email.replace(/\./g, ',');
		}
	}

	const syncData = () => {
		components = [];
		let idArray = [];
		const modifiedEmail = getEmail();
		firebase
			.database()
			.ref(modifiedEmail)
			.once('value')
			.then(snapshot => {
				snapshot.forEach(childSnapShot => {
					let data = childSnapShot.val();
					let key = childSnapShot.key;
					if (key !== 'count') {
						components.push(data);
						idArray.push(key);
					}
					totalCardsData(modifiedEmail).setItem(key, data);
				});
				initiate(idArray);
			});
	};

	useEffect(() => {
		const modifiedEmail = getEmail();
		let values = [];
		let keys = [];
		totalCardsData(modifiedEmail)
			.iterate((value, key) => {
				if (key !== 'count') {
					values.push(value);
					keys.push(key);
				}
			})
			.then(function () {
				if (values.length === 0) {
					if (modifiedEmail) {
						syncData();
					}
				} else {
					components = values;
					initiate(keys);
				}
			});
	}, []);

	async function initiate(id) {
		const modifiedEmail = getEmail();
		let variable = components.map((item, index) => {
			return <DashboardTestCard key={uuidv4()} data={item} id={id[index]} index={index} />;
		});

		setArr(variable);
		setCount(1);

		let downloadCount = firebase.database().ref(`${modifiedEmail}/count`);
		let snapshotDownloaded = await downloadCount.once('value');
		let snapShoutCount = snapshotDownloaded.val();
		if (components.length === snapShoutCount) return;
		syncData();
	}

	return (
		<>
			{count === 0 || paramId ? (
				<Loader />
			) : (
				<div className="content">
					<div className="container">
						<div className="page-title">Dashboard</div>
						<div className="page-subtitle">{arr ? arr.length : null} tests created</div>
						<div
							className="create-test-button"
							onClick={() => {
								setVisible(true);
								redirectToCreateTest();
							}}
						>
							<div className="create-test-button-group">
								<div className="button-add-large">
									<img alt="Create new test" src="/ic_add_plus.svg" />
								</div>
								<button className="button-primary-text">Create Test</button>
							</div>
						</div>
						{arr}
					</div>
					<Load visible={visible} />
				</div>
			)}
		</>
	);
};

export default dashboard;
