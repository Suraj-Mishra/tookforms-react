import firebase from 'firebase/app';
import 'firebase/database';
import initFirebase from '../../../utils/auth/initFirebase';

initFirebase();

export default async (req, res) => {
	const { id } = req.query;

	firebase
		.database()
		.ref('/test/' + id)
		.once('value')
		.then(snapshot => {
			const data = snapshot.val();
			res.status(200).json(data);
		})
		.catch(err => console.log(err));
};
