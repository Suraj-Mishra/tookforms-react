const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

exports.calc = functions.https.onCall(async (data, context) => {
	const [testId, testData, userName, email] = data;

	let score = 0;
	let totalQuestions = 0;
	let passingScore;
	let title;

	const referenceAnswersRequest = admin.database().ref(`answers/${testId}`);
	const data3 = await referenceAnswersRequest.once('value');
	const referenceAnswers = data3.val();

	testData.forEach((element, index) => {
		referenceAnswers[index][1].forEach((something, index1) => {
			if (element[1] === something.option) {
				score += 1;
			}
		});
		totalQuestions += 1;
	});

	passingScore = referenceAnswers.passingPercentage;

	title = referenceAnswers.title;

	function percentage(score, totalQuestions) {
		let result = (100 * score) / totalQuestions;
		return result >= passingScore ? 'Passed' : 'Failed';
	}

	let passingResult = percentage(score, totalQuestions);

	if (passingResult === 'Passed') {
		admin
			.firestore()
			.collection('testResult')
			.doc(`${testId}`)
			.get()
			.then(doc => {
				if (doc.exists) {
					admin
						.firestore()
						.collection('testResult')
						.doc(`${testId}`)
						.update({
							data: admin.firestore.FieldValue.arrayUnion({ userName, email }),
						});
				} else {
					admin
						.firestore()
						.collection('testResult')
						.doc(`${testId}`)
						.set({
							data: [{ userName, email }],
						});
				}
				return;
			})
			.catch(error => console.log(error));
	} else {
		admin
			.firestore()
			.collection('failedResults')
			.doc(`${testId}`)
			.get()
			.then(doc => {
				if (doc.exists) {
					admin
						.firestore()
						.collection('failedResult')
						.doc(`${testId}`)
						.update({
							data: admin.firestore.FieldValue.arrayUnion({ userName, email }),
						});
				} else {
					admin
						.firestore()
						.collection('failedResult')
						.doc(`${testId}`)
						.set({
							data: [{ userName, email }],
						});
				}
				return;
			})
			.catch(error => console.log(error));
	}

	admin
		.firestore()
		.collection('mail')
		.add({
			to: email,
			template: {
				name: 'result',
				data: {
					name: userName,
					status: passingResult,
					testTitle: title,
					// testDescription: info.testDescription,
				},
			},
		})
		.then(() => console.log('Queued email for delivery!'))
		.catch(err => console.log(err));
});
