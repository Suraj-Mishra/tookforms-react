import { useEffect, useState, useContext, createContext } from 'react';
import nookies, { destroyCookie } from 'nookies';
import firebase from 'firebase/app';
import 'firebase/auth';
import initFirebase from '../auth/initFirebase';

const AuthContext = createContext({});

export const AuthProvider = ({ children }) => {
	initFirebase();

	const [user, setUser] = useState();

	useEffect(() => {
		return firebase.auth().onIdTokenChanged(async user => {
			if (user) {
				const token = await user.getIdToken();
				setUser(user);
				nookies.set(undefined, 'token', token, { path: '/', maxAge: 60 * 60 });
				nookies.set(undefined, 'user', JSON.stringify(user), { path: '/', maxAge: 60 * 60 });
			}
		});
	}, []);

	return <AuthContext.Provider value={{ user }}>{children}</AuthContext.Provider>;
};

export const useAuth = () => useContext(AuthContext);
